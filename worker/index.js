'use strict';

const Agenda = require('agenda');
const request = require('request');
const models = require('../models');
const producer = require('../amqp/producer');
const dictionary = require('../dictionary');
const steamApiConfig = require('../config')['steam-api'];

const { TrackingRequest, UserStats } = models;

const url = `http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=${steamApiConfig.appid}&key=${steamApiConfig.key}&steamid=`;

// get user stats from api
function getStats(steamid) {
    return new Promise((resolve, reject) => {
        request.get(url + steamid, function (err, res, body) {
            if (err) {
                return reject(err);
            }
            const statusCode = res.statusCode;
            if (statusCode !== 200) {
                return reject(`StatusCode: ${statusCode}`);
            }
            const result = Object.create(null);
            // parse body, get stats and make a single object (result) out of the array of objects
            // [{ name : value }] -> { name : value }
            Object.entries(JSON.parse(body).playerstats.stats).forEach(kv => {
                result[kv[1].name] = kv[1].value;
            });
            return resolve(result);
        });
    });
}

function getStatsDiff(old, cur) {
    const diff = Object.create(null);
    for (let key in cur) {
        try {
            if (old[key]) {
                const diffVal = cur[key] - old[key];
                if (diffVal && diffVal !== 0) {
                    diff[key] = diffVal;
                }
            } else {
                const curVal = cur[key];
                if (curVal && curVal !== 0) {
                    diff[key] = curVal;
                }
            }
        } catch { }
    }
    return diff;
}

async function createUserStats(steamid) {
    const statsData = JSON.stringify(await getStats(steamid));
    return new Promise((resolve, reject) => {
        UserStats.findOne({ steamid }, function (err, oldStats) {
            if (err) {
                return reject(`DATABASE ERROR: ${err}`);
            }
            if (oldStats) {
                return resolve();
            }
            UserStats.create({
                steamid,
                stats: statsData,
            }, function (err) {
                if (err) {
                    return reject(`DATABASE ERROR: ${err}`);
                }
                resolve();
            });
        });
    });
}

// get user stats from api
// save to database
async function updateUserStats(steamid) {
    const statsData = JSON.stringify(await getStats(steamid));
    return new Promise((resolve, reject) => {
        const stats = {
            steamid,
            stats: statsData,
        };
        UserStats.findOneAndUpdate({ steamid }, stats, { upsert: true }, function (err, oldStats) {
            if (err) {
                return reject(`DATABASE ERROR: ${err}`);
            }
            resolve({ oldStats, stats });
        });
    });
}

async function processUser(steamid, trackingRequests) {
    const { oldStats, stats } = await updateUserStats(steamid);
    if (!oldStats) {
        return;
    }
    const oldStatsDict = JSON.parse(oldStats.stats);
    const statsDict = JSON.parse(stats.stats);
    const updates = getStatsDiff(oldStatsDict, statsDict);
    // if diffs count > 0
    if (Object.keys(updates).length > 0) {
        // procces every tracking request
        for (let tr of trackingRequests) {
            const data = {};
            for (let field in updates) {
                if (tr.fields.includes(field)) {
                    const universeName = dictionary.ltu[field];
                    if (universeName) {
                        data[universeName] = updates[field];
                    }
                }
            }
            // send updates
            if (Object.keys(data).length > 0) {
                producer.sendUpdates(tr.id, data, Date.now() - oldStats.updatedAt.getTime());
            } 
        }
    }
}

// update all users every minute
async function start() {
    const agenda = new Agenda().mongo(models.connection.connection.db);
    const exit = () => {
        agenda.stop(() => process.exit(0));
    }
    process.on('SIGTERM', exit);
    process.on('SIGINT' , exit);

    agenda.define('update stats', function (job, done) {
        TrackingRequest.find().lean().exec(function (err, trs) {
            if (err) {
                console.error(`DATABASE ERROR: ${err}`);
                done(err);
                return;
            }

            const userRequests = new Map();
            // create map { steamid : trackingRequest }
            for (let tr of trs) {
                const steamid = tr.steamid;
                if (userRequests.has(steamid)) {
                    userRequests.get(steamid).add(tr); 
                } else {
                    const requests = new Set();
                    requests.add(tr);
                    userRequests.set(steamid, requests);
                }
            }
            
            // process each user
            userRequests.forEach((trs, steamid) => {
                processUser(steamid, [...trs]);
            });

            done();
        });
    });

    await agenda.start();
    await agenda.every('1 minutes', 'update stats');
    console.log('WORKER STARTED');
}

module.exports = {
    createUserStats,
    updateUserStats,
    processUser,
    start,
};