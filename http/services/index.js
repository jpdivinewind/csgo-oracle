'use strict';

module.exports = {
    connect(app) {
        require('./dictionary.service').connect(app);
    },
};