'use strict';

// store keys from utl (universe to local) dictionary object
const dictionary = Object.keys(require('../../dictionary').utl);

function getDictionary(req, res) {
    res.status(200).json(dictionary);
}

module.exports = {
    connect(app) {
        app.get('/api/dictionary', getDictionary);
    },
};