'use strict';

const app = require('express')();
const requestLogger = require('morgan');
const config = require('../config');

// connect services
require('./services').connect(app);

// request logger
app.use(requestLogger('tiny'));

module.exports = {
    start() {
        // start server
        return new Promise((resolve, reject) => {
            const { PORT } = config;
            app.listen(PORT, () => {
                console.log(`HTTP SERVER STARTED AT PORT: ${PORT}`);
                resolve(app);
            }).on('error', err => {
                const message = `CRITICAL HTTP SERVER ERROR: ${err}`;
                console.error(message);
                reject(new Error(message));
            });
        });
    },
};