'use strict';

// universal to local
const utl = {
    'headshots': 'total_kills_headshot',
    'rounds': 'total_rounds_played',
};

// local to universal
const ltu = { };

// calculate ltu from utl
Object.entries(utl).forEach(kv => {
    ltu[kv[1]] = kv[0];
});

module.exports = { utl, ltu };