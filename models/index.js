'use strict';

const mongoose = require('mongoose');
const config = require('../config').mongo;
const fs = require('fs').promises;
const path = require('path');

module.exports = {
    // connect to database and load all models
    async connect() {
        try {
            this.connection = await mongoose.connect(config.uri, config.options);
        } catch (err) {
            throw new Error(`ERROR CONNECTING TO DATABASE: ${err}`);
        }
        console.log('CONNECTED TO DATABASE');
        // models are taken from files in the current directory
        const files = await fs.readdir(__dirname);
        for (let file of files) {
            if (file === 'index.js') {
                continue;
            }
            // all model files must export the function with the following signature: (Mongoose) => Model
            const model = require(path.resolve(__dirname, file))(mongoose);
            const name = file.substring(0, file.length - 3);
            // add a model to this module using the key obtained from the file name
            this[name] = model;
            // now you can write: require('./models').ExampleModel
        }
    },
};