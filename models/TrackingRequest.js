'use strict';

module.exports = function (mongoose) {
    // id - unique id
    // steamid - steam user id
    // fields - array of field names that are tracked (local field names)
    const schema =  new mongoose.Schema({
        id: String,
        steamid: String,
        fields: [String],
    });
    return mongoose.model('TrackingRequest', schema);
};