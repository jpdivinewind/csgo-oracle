'use strict';

module.exports = function (mongoose) {
    // steamid - steam user id
    // stats - stats dictionary in json string
    const schema =  new mongoose.Schema({
        steamid: String,
        stats: String,
    }, {
        timestamps: {
            updatedAt: 'updatedAt',
        },
    });
    return mongoose.model('UserStats', schema);
};