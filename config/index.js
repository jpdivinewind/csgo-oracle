'use strict';

const nconf = require('nconf');
const configFileData = require('./config');

// default config from file
const defaults = configFileData['defaults'];
// current config from file
const config = configFileData[process.env.NODE_ENV || 'development'];

function merge(obj, defaults) {
    for (let defaultKey in defaults) {
        if (defaultKey in obj) {
            const t = typeof defaults[defaultKey];
            if (t === 'object' && typeof obj[defaultKey] === t) {
                if (!Array.isArray(defaults[defaultKey]) && !Array.isArray(obj[defaultKey])) {
                    if (obj[defaultKey] !== null) {
                        merge(obj[defaultKey], defaults[defaultKey]);
                    }
                }
            }
        } else {
            obj[defaultKey] = defaults[defaultKey];
        }
    }
}

// merge config from file with default config from file
merge(config, defaults);

module.exports = nconf.argv().env().defaults(config).get();