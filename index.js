'use strict';

async function start() {
    await require('./models').connect(); // database
    await require('./http').start(); // express app
    await require('./amqp/producer').start(); // amqp producer
    await require('./worker').start(); // api worker
    await require('./amqp/consumer').start(); // amqp consumer
}

start().catch(err => {
    console.error(`CRITICAL ERROR: ${err}`);
    process.exit(1);
});