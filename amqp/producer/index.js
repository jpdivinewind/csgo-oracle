'use strict';

const mq = require('amqplib');
const config = require('../../config').amqp;

let channel;
const updatesQueue = config.outQueueName;
const errorsQueue = config.outErrorQueueName;

module.exports = {
    async start() {
        let connection;
        try {
            connection = await mq.connect(config.uri, config.options);
        } catch (err) {
            throw new Error(`AMQP PRODUCER CONNECTION ERROR: ${err}`);
        }
        console.log('AMQP PRODUCER CONNECTED');
        channel = await connection.createChannel();
        await channel.assertQueue(updatesQueue);
        await channel.assertQueue(errorsQueue);
    },
    sendUpdates(id, updates, deltaTime) {
        const buffer = Buffer.from(JSON.stringify({
            id,
            updates,
            deltaTime,
        }));
        channel.sendToQueue(updatesQueue, buffer);
    },
    sendError(error) {
        const buffer = Buffer.from(JSON.stringify(error));
        channel.sendToQueue(errorsQueue, buffer);
    },
}