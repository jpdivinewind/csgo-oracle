'use strict';

const mq = require('amqplib');
const config = require('../../config').amqp;
const dictionary = require('../../dictionary');
const producer = require('../producer');
const worker = require('../../worker');

const { TrackingRequest, UserStats } = require('../../models');

let channel;

async function startTracking(message, id, steamid, fields) {
    // check input
    if (!id || !fields || !steamid) {
        // send error
        producer.sendError({
            'action': 'startTracking',
            'id': id,
            'message': 'One of the required fields is missing: id, fields, userId',
        });
        channel.nack(message, false, false);
        return;
    }
    // replace universal field names with local field names
    fields = fields.map(field => dictionary.utl[field]).filter(field => field);
    // create tracking request
    const trackingRequest = {
        id,
        steamid,
        fields,
    };
    TrackingRequest.create(trackingRequest, async function (err) {
        if (err) {
            console.error(`DATABASE ERROR: ${err}`);
            channel.nack(message, false, true);
            return;
        }
        // just create (if not exists) user statistics in the database (with 'true')
        // if error - send error and remove tracking request
        try {
            await worker.createUserStats(steamid);
            channel.ack(message);
        } catch (err) {
            // send error
            producer.sendError({
                'action': 'startTracking',
                'id': id,
                'message': `updateUserStats Error: ${err}`,
            });
            channel.nack(message, false, false);
            TrackingRequest.findOneAndRemove({ id }, function (err) {
                console.error(`Can't remove invalid tracking request. id: "${id}"`);
            });
        }
    });
    return;
}

function stopTracking(message, id) {
    // check input
    if (!id) {
        // send error
        producer.sendError({
            'action': 'stopTracking',
            'id': id,
            'message': 'Required field is missing: id',
        });
        channel.nack(message, false, false);
        return;
    }
    // remove tracking request
    TrackingRequest.findOneAndRemove({ id }, function (err) {
        if (err) {
            console.error(`DATABASE ERROR: ${err}`);
            channel.nack(message, false, true);
            return;
        }
        // find all tracking requests by this user
        TrackingRequest.find({ steamid }).lean().exec(function (err, trs) {
            if (err) {
                console.error(`DATABASE ERROR: ${err}`);
                channel.nack(message, false, true);
                return;
            }
            if (trs.length > 0 ) {
                channel.ack(message);
                return;
            }
            // remove user statistics if the number of tracking requests is 0
            UserStats.findOneAndRemove({ steamid }, function (err) {
                if (err) {
                    console.error(`DATABASE ERROR: ${err}`);
                    channel.nack(message, false, true);
                    return;
                }
                channel.ack(message);
            });
        });
    });
}

// message handler
async function handler(message) {
    const content = JSON.parse(message.content.toString());
    let { action, id, fields } = content;
    const steamid = content.userId;

    if (action === 'startTracking') {
        return await startTracking(message, id, steamid, fields);
    }
    
    if (action === 'stopTracking') {
        return stopTracking(message, id);
    }

    // invalid action
    producer.sendError({
        'action': action,
        'id': id,
        'message': 'Invalid action',
    });
    channel.nack(message, false, false);
}

module.exports = {
    async start() {
        let connection;
        try {
            connection = await mq.connect(config.uri, config.options);
        } catch (err) {
            throw new Error(`AMQP CONSUMER CONNECTION ERROR: ${err}`);
        }
        console.log('AMQP CONSUMER CONNECTED');
        channel = await connection.createChannel();
        const queue = config.inQueueName;
        await channel.assertQueue(queue);
        await channel.consume(queue, handler);
    },
}